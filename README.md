# Project Bus 2.0 - Android

Project Bus trata-se de um aplicativo para Android (e futuramente para iOS) utilizando as informações obtidas pelos webservices da EMTU para prover informações importantes para os usuário.
O projeto 2.0 adapta o projeto antigo para seguir as boas praticas de programação para Android e usa Kotlin como linguagem.