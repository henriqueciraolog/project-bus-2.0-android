package br.com.hciraolo.projectbus.repositorys

import android.content.Context
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class EmtuRepository(val context: Context) : IEmtuRepository {

    companion object {
        private val emtuApi = Retrofit.Builder()
            .baseUrl("http://200.144.29.93:8088/")
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build().create(EmtuApi::class.java)
    }

    override fun getListaDeLinhas() = emtuApi.getLines()

    override fun getInformacoesGestecDaLinha(lineCode: String, lineDirection: String) = emtuApi.getLineDetails(lineCode, lineDirection)
}