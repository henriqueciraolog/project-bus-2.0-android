package br.com.hciraolo.projectbus.repositorys

import io.reactivex.Single

interface IEmtuRepository {
    fun getListaDeLinhas(): Single<ListLineResponse>
    fun getInformacoesGestecDaLinha(codigoLinha: String, sentidoLinha: String): Single<ListLineResponse>
}