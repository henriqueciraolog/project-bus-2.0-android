package br.com.hciraolo.projectbus.repositorys

import io.reactivex.Single
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface EmtuApi {
    @Headers("Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw")
    @GET("services.svc/linhas/getListaDeLinhas")
    fun getLines(): Single<ListLineResponse>

    @Headers("Authorization: Basic cm9iby1lbXR1OmVtdHUxMDMw")
    @GET("services.svc/linhas/getInformacoesGestecDaLinha")
    fun getLineDetails(@Query("codigoLinha") codigoLinha: String, @Query("sentidoLinha") sentidoLinha: String): Single<ListLineResponse>
}