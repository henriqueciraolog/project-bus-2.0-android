package br.com.hciraolo.projectbus.repositorys

data class LineDetailsResponse (
    val schedule: Schedule,
    val sectionings: List<Sectioning>,
    val travelTime: Int,
    val fare: Double
)

data class Schedule(
    var weekDays: List<String>,
    var sundayAndHolidays: List<String>,
    var saturday: List<String>
)

data class Sectioning(
    var sectionIdentfy: Int,
    var patchName: String,
    var fare: Double
)