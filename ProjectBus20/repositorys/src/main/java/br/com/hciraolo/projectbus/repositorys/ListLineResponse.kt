package br.com.hciraolo.projectbus.repositorys

data class ListLineResponse (
    val lines : List<LineDto>
)

data class LineDto (
    val chElemento: Int,
    val chRegiao: Int,
    val codigo: String,
    val destino: String,
    val origem: String,
    val sentido: String
)