package br.com.hciraolo.projectbus.repositorys

interface INoxxonsatRepository {
    fun getLineInfoLocalization(lineNumber: String, withItinerate: Boolean)
}