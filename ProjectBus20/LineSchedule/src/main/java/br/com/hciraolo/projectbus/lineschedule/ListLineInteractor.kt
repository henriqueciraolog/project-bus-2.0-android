package br.com.hciraolo.projectbus.lineschedule

import android.content.Context
import br.com.hciraolo.projectbus.repositorys.EmtuRepository
import br.com.hciraolo.projectbus.repositorys.IEmtuRepository
import br.com.hciraolo.projectbus.repositorys.LineDto
import br.com.hciraolo.projectbus.repositorys.ListLineResponse
import io.reactivex.Single

class ListLineInteractor(context: Context) {

    val emtuRepository : IEmtuRepository = EmtuRepository(context)

    fun getLines() : Single<List<LineDto>> = emtuRepository.getListaDeLinhas().map {
        it.lines
    }
}