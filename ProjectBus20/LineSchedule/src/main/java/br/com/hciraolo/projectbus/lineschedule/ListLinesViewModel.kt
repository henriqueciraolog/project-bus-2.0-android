package br.com.hciraolo.projectbus.lineschedule

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import br.com.hciraolo.projectbus.repositorys.LineDto
import br.com.hciraolo.projectbus.repositorys.Schedule
import io.reactivex.schedulers.Schedulers

class ListLinesViewModel(app: Application) : AndroidViewModel(app) {
    private val interactor: ListLineInteractor = ListLineInteractor(app)

    var getLineCallback: MutableLiveData<List<LineDto>> = MutableLiveData()
    var errors: MutableLiveData<String> = MutableLiveData()

    fun getLines() {
        interactor.getLines()
            .subscribeOn(Schedulers.newThread())
            .doOnError {
                errors.postValue(it.message)
            }
            .subscribe { lines ->
                getLineCallback.postValue(lines)
            }
    }
}
