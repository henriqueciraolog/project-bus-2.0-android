package br.com.hciraolo.projectbus.lineschedule

import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import br.com.hciraolo.projectbus.repositorys.LineDto
import kotlinx.android.synthetic.main.list_lines_fragment.*
import java.util.ArrayList


class ListLinesFragment : Fragment(), LineRecyclerViewAdapter.OnItemClickListerner {

    companion object {
        fun newInstance() = ListLinesFragment()
    }

    private lateinit var viewModel: ListLinesViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.list_lines_fragment, container, false)
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        viewModel = ViewModelProviders.of(this).get(ListLinesViewModel::class.java)
        viewModel.getLineCallback.observe(this, Observer { updateRecyclerView(it) })
        setupRecycleView()
    }

    private fun setupRecycleView() {
        val rcvLayoutManager = LinearLayoutManager(context)
        rcvLines.layoutManager = rcvLayoutManager
        rcvLines.adapter =  LineRecyclerViewAdapter(this)
        val dividerItemDecoration =
            DividerItemDecoration(rcvLines.context, LinearLayout.VERTICAL)
        rcvLines.addItemDecoration(dividerItemDecoration)
    }

    fun updateRecyclerView(lines: List<LineDto>) {
        (rcvLines.adapter as LineRecyclerViewAdapter).updateList(lines)
    }

    override fun onItemClick(linha: LineDto) {

    }

}
