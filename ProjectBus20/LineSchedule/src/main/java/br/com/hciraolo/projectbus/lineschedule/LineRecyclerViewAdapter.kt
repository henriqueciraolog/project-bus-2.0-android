package br.com.hciraolo.projectbus.lineschedule

import android.text.TextUtils
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.hciraolo.projectbus.repositorys.LineDto
import kotlinx.android.synthetic.main.item_line.view.*
import java.util.ArrayList

class LineRecyclerViewAdapter(val mListener: OnItemClickListerner) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mDataset: ArrayList<LineDto> = ArrayList()

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): RecyclerView.ViewHolder {
        val v = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_line, parent, false)
        return LineViewModel(v)
    }

    // Replace the contents of a view (invoked by the layout manager)
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val linha = mDataset[position]

        holder.itemView.lnlLinha.setOnClickListener {
            mListener.onItemClick(
                mDataset[position]
            )
        }
        var codLinha = linha.codigo
        var servComp = ""
        if (codLinha.length > 4) {
            servComp = codLinha.substring(4)
            codLinha = codLinha.substring(0, 4)
        }

        holder.itemView.txvCodLinha.setText(codLinha)
        holder.itemView.txvServComp.text = servComp
        if (servComp.length > 0)
            holder.itemView.txvServComp.visibility = View.VISIBLE
        else
            holder.itemView.txvServComp.visibility = View.GONE
        holder.itemView.txvOrigemLinha!!.setText(
            linha.origem.replace(
                "TERMINAL",
                "TERM."
            ).replace("RODOVIARIO", "RODOV.")
        )
        holder.itemView.txvOrigemLinha.ellipsize = TextUtils.TruncateAt.MARQUEE
        holder.itemView.txvOrigemLinha.isSingleLine = true
        holder.itemView.txvOrigemLinha.marqueeRepeatLimit = 5
        holder.itemView.txvOrigemLinha.isSelected = true
        holder.itemView.txvDestinoLinha.setText(
            linha.destino.replace(
                "TERMINAL",
                "TERM."
            ).replace("RODOVIARIO", "RODOV.")
        )
        holder.itemView.txvDestinoLinha.ellipsize = TextUtils.TruncateAt.MARQUEE
        holder.itemView.txvDestinoLinha.isSingleLine = true
        holder.itemView.txvDestinoLinha.marqueeRepeatLimit = 5
        holder.itemView.txvDestinoLinha.isSelected = true
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount(): Int {
        return mDataset!!.size
    }

    fun updateList(newlist: List<LineDto>) {
        mDataset = ArrayList<LineDto>()
        mDataset!!.addAll(newlist)
        notifyDataSetChanged()
    }

    interface OnItemClickListerner {
        fun onItemClick(linha: LineDto)
    }
}

class LineViewModel(v: View) : RecyclerView.ViewHolder(v)